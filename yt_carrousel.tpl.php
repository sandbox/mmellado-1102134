<?
/**
 * yt_carrousel.tpl.php
 * Marcos Mellado <mmellado@onixmedia.net>
 *
 * Template file to render the YouTube Carrousel Block.
 * Be very careful when modifying this file. Take into consideration that the ids and classes of the items
 * in this file are important for the engine javascript to work. If necessary, add anything to the file.
 *
 * VARIABLES
 * 
 * $usr_key 	:	Flag that indicates if the feed was correctly generated
 * $num_vids 	:	Indicates the number of videos brought by the feed
 * $ids[]		:	Array with the YouTube video IDs of all the videos brought by the feed
 * $thumbs[]	:	Array with the YouTube video thumbnails of all the videos brought by the feed
 * $titles[]	:	Array with the YouTube video titles of all the videos brought by the feed
 *
 */ 
?>

<? if ( $usr_key ): ?>

<div id="yt-carrousel-block">
	<? for($i=0; $i <= $num_vids; $i++): ?>
	<? 	
			if( $i == 0 )
			{
	?>
			<div id="main-video">
				<h3>VIDEOS</h3>
				<object type="application/x-shockwave-flash" style="width:280px; height:200px;" data="http://www.youtube.com/v/<?=$ids[$i]?>"><param name="movie" value="http://www.youtube.com/v/<?=$ids[$i]?>" /></object>
			</div> <!--- /#main-video -->
			<div class="spacer">&nbsp;</div>
			<div class="scrollable-video-content">
				<a class="prev browse left"></a>
				<div id="scrollable-video-thumbs">
					<div class="video-items">
						<div class="video-thumb active">
							<img src="<?=$thumbs[$i]->url?>" alt="<?=$titles[$i]?>" rel="<?=$ids[$i]?>" />
							<span><?=$titles[$i]?></span>
							<div class="hover">&nbsp;</div>
						</div><!--- /.video-thumb -->
			
	<?
			}//if( $i == 0 )
			else
			{
	?>
						<div class="video-thumb">
							<img src="<?=$thumbs[$i]->url?>" alt="<?=$titles[$i]?>" rel="<?=$ids[$i]?>" />
							<span><?=$titles[$i]?></span>
							<div class="hover">&nbsp;</div>
						</div><!--- /.video-thumb -->
	<?		
			}//else
	?>
	
	<? endfor; ?>
	
					</div><!-- /.video-items -->
				</div><!-- /scrollable-video-thumbs -->
				<a class="next browse right"></a>
			</div><!-- /scrollable-video-content -->
	 
</div><!-- /yt-carrousel-block -->

<? else: ?>

<div id="yt-carrousel-block">

	<h3>Videos</h3>
	<div id="main-video" class="empty">
		<p>
			<?= t('Sorry, there are no videos available.'); ?>
		</p>
	</div>
</div>

<? endif; ?>

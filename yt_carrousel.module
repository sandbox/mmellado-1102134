<?

/**
 * yt_carrousel.module
 * Marcos Mellado <mmellado@onixmedia.net>
 *
 * Creates a simple carrousel out of the YouTube videos using the YouTube API.
 * Videos can be selected either from a YouTube user or from a public YouTube playlist.
 *
 */



/**
 * Implementation of hook menu.
 * Define the configuration page for the block.
 *
 */
function yt_carrousel_menu() {

  $items['admin/config/youtube_carrousel'] = array(
	'title' => 'YouTube Carrousel',
    'description' => 'YouTube carrousel settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('yt_carrousel_settings'),
    'access arguments' => array('YouTube Carrousel'),
	);

  return $items;
}

/**
 * Implementation of hook perm.
 * Set permissions to configure the block.
 *
 */
function yt_carrousel_perm() {
  $items = array('Configure the YouTube carrousel');
  return $items;
}


/**
 * This function defines the different fields in the block configuration page.
 */
function yt_carrousel_settings() {

  // Developer Key
  $form['yt_dev_key'] = array(
	'#type' => 'textfield',
	'#title' => 'Developer Key',
	'#default_value' => variable_get('yt_dev_key', ''),
	'#required' => TRUE,
	'#description' => 'The YouTube Developer Key for the app. You can go to http://code.google.com/apis/youtube/dashboard to create one.'
	);


  //Feed type (user or playlist)
  $form['yt_ftype'] = array(
      '#type' => 'radios',
      '#title' => t('Feed Type'),
      '#default_value' => variable_get('yt_ftype', 0),
      '#options' => array(t('User'), t('Playlist')),
      '#required' => TRUE,
      '#description' => 'If you choose Playlist, the block will work only with public playlists.'
    );

  //If user, the username to get the videos from
  $form['yt_account'] = array(
	'#type' => 'textfield',
	'#title' => 'YouTube Account',
	'#default_value' => variable_get('yt_account', ''),
	'#description' => 'The YouTube username to get the videos from'
	);


  //If user, the order for the videos to be ordered in the feed.
  $form['yt_uorder'] = array(
   '#type' => 'select',
      '#title' => t('Videos Order'),
      '#default_value' => variable_get('yt_uorder', 'published'),
   '#options' => array(
        'published' => t('Published Date'),
        'relevance' => t('Relevance'),
        'viewCount' => t('View Count'),
        'rating' => t('Rating'),
      ),
    );


  //If playlist, the playlist ID
  $form['yt_playlist'] = array(
	'#type' => 'textfield',
	'#title' => 'YouTube playlist ID.',
	'#default_value' => variable_get('yt_playlist', ''),
	'#description' => 'This can be found on the playlist information link as the "p" parameter. Make sure the playlist ID is of a public playlist.'
	);


  //If playlist, the order for the videos to be ordered in the feed.
  $form['yt_porder'] = array(
   '#type' => 'select',
      '#title' => t('Playlist Order'),
      '#default_value' => variable_get('yt_porder', 'position'),
   '#options' => array(
        'position' => t('Position'),
        'commentCount' => t('Comment Count'),
        'duration' => t('Duration'),
        'published' => t('Published Date'),
        'title' => t('Title'),
		'viewCount' => t('View Count'),
      ),
    );

  return system_settings_form($form);
}

/**
 * Implementation of hook_theme()
 * Here we create the .tpl.php to render the videos.
 *
 */
function yt_carrousel_theme() {
  $arr['yt_carrousel'] = array(
    'template' => 'yt_carrousel',
    'arguments' => array(),
  );
  return $arr;
}

/**
 * Implementation of hook_block()
 * Here we create the block itself.
 *
 */
function yt_carrousel_block($op = 'list', $delta = 0, $edit = array()) {
  switch ($op) {
    //Set name for the block in the blocks list
    case 'list':
      $blocks[0]['info'] = t('YouTube Carrousel');
      return $blocks;
      break;

      //Include the necessary CSS and JS Files for the carrousel to work and the videos to be
      //correctly rendered.
    case 'view':
      switch ($delta) {
        case 0:
          drupal_add_css(drupal_get_path('module', 'yt_carrousel') . '/yt_carrousel.css');
          drupal_add_js(drupal_get_path('module', 'yt_carrousel') . '/jquery.tools.min.js');
          drupal_add_js(drupal_get_path('module', 'yt_carrousel') . '/yt_carrousel.engine.js');

          $blocks['content'] = theme('yt_carrousel', array()); //Set the content of the block to the yt_carrousel.tpl.php file.

          break;
      }
      break;
  }
  return $blocks;
}

/**
 * Implementation of preprocess hook
 * Here we prepare the variables that will be used in the template file
 * This variables will be only useful in the yt_carrousel.tpl.php file
 *
 */
function yt_carrousel_preprocess_yt_carrousel(&$vars, $hook) {

  global $user; //get the user information to check if its the admin later on.

  $type = variable_get('yt_ftype', 0); //check the type of feed to be used.


  if ( $type == 0 ) {
    //get the feed in json format, setting the yt username, feed order and developer key
    $yt_json = drupal_http_request('http://gdata.youtube.com/feeds/api/videos?alt=json&orderby=' . variable_get('yt_uorder', 'published') . '&author=' . variable_get('yt_account', '') . '&key=' . variable_get('yt_dev_key', ''));
  }
  else if ( $type == 1 ) {
    //get the feed in json format, setting the yt playlist id, and feed order
    $yt_json = drupal_http_request('http://gdata.youtube.com/feeds/api/playlists/' . variable_get('yt_playlist', '') . '?v=2&alt=json&orderby=' . variable_get('yt_porder', 'position'));
  }

  $yt_json = json_decode($yt_json->data);

  // if the feed was successfully received
  if ( $yt_json ) {

    $vars['usr_key'] = true; //set flag for template to know if there will be videos or not.

    $ids = array();
    $thumbs = array();
    $titles = array();

    foreach ( $yt_json->feed->entry as $yt ) {
      $titles[] = $yt->title->{'$t'};
      $thumbs[] = $yt->{'media$group'}->{'media$thumbnail'}[0];
      //ids are gathered in a different way for both kind of feeds
      if ( $type == 0 ) {
        $id = split('/', $yt->id->{'$t'});
        $ids[] = $id[6];
      }
      else if ( $type == 1 ) {
        $ids[] = $yt->{'media$group'}->{'yt$videoid'}->{'$t'};
      }

    }

    $vars['titles'] = $titles;
    $vars['thumbs'] = $thumbs;
    $vars['ids'] = $ids;
    $vars['num_vids'] = count($ids) -1;
  }
  else {
    if ( $user->uid  == 1 ) {
      drupal_set_message(t('It is necessary to configure the YouTube Carrousel. Go to the ') . l('YouTube Carrousel settings', 'admin/config/youtube_carrousel') . t(' to do this.'));
    }
  }

}

/**
 * Implementation of hook_init()
 * Actions to execute before the page is loaded.
 *
 */
function yt_carrousel_init() {
  //If in the carrousel config page, add a js to give an easier user experience
  if ( arg(0) == 'admin' && arg(1) == 'settings' && arg(2) == 'youtube_carrousel' ) {
    drupal_add_js(drupal_get_path('module', 'yt_carrousel') . '/yt_carrousel.admin.js');
  }
}


Drupal.behaviors.yt_carrousel = function()
{
	
	//If a thumbnail is clicked, remove the original object and replace it with a new one using
	//the corresponding video id.
	$('.video-thumb').click(
   		function()
   		{
   			$('.video-thumb').removeClass('active');
   			$(this).addClass('active');
   			var video = $('img', this).attr('rel');
   			$('#main-video object').remove();
   			$('#main-video').append('<object type="application/x-shockwave-flash" style="width:280px; height:200px;" data="http://www.youtube.com/v/'+video+'&autoplay=1"><param name="movie" value="http://www.youtube.com/v/'+video+'&autoplay=1" /></object>');
   		}
   	);
   	
   	
   	//hover effect for thumbnails
   	$('.video-thumb').hover(
		function(){
			$('.hover', this).show();
		},
		function(){
			$('.hover', this).hide();
		}
	);
	
	
	//scrollable functionality for the thumbnails
	$("#scrollable-video-thumbs").scrollable();

}
Drupal.behaviors.yt_carrousel_admin = function()
{	
	var status = $('.form-radios input:checked').val();
	if( status == 0 )
	{
		$('#edit-yt-playlist-wrapper, #edit-yt-porder-wrapper').hide();
	}
	else if ( status == 1 )
	{
		$('#edit-yt-account-wrapper, #edit-yt-uorder-wrapper').hide();
	}
	
	$('.form-radios input').click(function(){
		//alert($('.form-radios input:checked').val());
		status = $('.form-radios input:checked').val();
		if( status == 0 )
		{
			$('#edit-yt-playlist-wrapper, #edit-yt-porder-wrapper').hide('slow');
			$('#edit-yt-account-wrapper, #edit-yt-uorder-wrapper').show('slow');
		}
		else if ( status == 1 )
		{
			$('#edit-yt-account-wrapper, #edit-yt-uorder-wrapper').hide('slow');
			$('#edit-yt-playlist-wrapper, #edit-yt-porder-wrapper').show('slow');
		}
	});
	
	
}